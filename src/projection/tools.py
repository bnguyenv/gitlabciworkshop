import numpy as np


def stereographic_projection(coordinates):
    """Computes the stereographic projection of a point cloud in :math:`{R^N}` to a
    point cloud in :math:`{R^(N-1)}`.

    Parameters
    ----------
    coordinates : ndarray
        The point coordinates in :math:`{R^N}`.

    Raises
    ------
    ZeroDivisionError
        If the last element of coordinates is equal to 1.

    Returns
    -------
    ndarray
        Returns the point coordinates in :math:`{R^(N-1)}`.
    """
    # Prepare (1. / (1. - t))
    denominator = 1. - coordinates[::, -1]
    any_div = (denominator == np.zeros(denominator.shape[0]))
    # Exception if any (1. - t) is zero
    if np.any(any_div):
        raise ZeroDivisionError("float division by zero")

    # Return [(x / (1. - t)), (y / (1. - t)), ...]
    denominator = denominator.reshape(coordinates.shape[0], 1)
    pts_proj = np.delete(coordinates, -1, axis=1)

    return pts_proj / denominator
