# GitLabCIWorkshop

The purpose of this project is to introduce GiltLabCI at the Engineers' Day of Saclay 2024 (aka. "Journée des Ingénieurs")

## Getting started

GitlabCI presentation - 10 mn

## Python project

The Python code is only composed of a small function that computes the stereographic projection of a point cloud in $`R^4`$ to a point cloud in $`R^3`$:

$`P(x, y, z, t) \Rightarrow P(x', y', z')`$ where $`x'=x/(1-t), y'=y/(1-t), z'=z/(1-t)`$

Example:
```python
from projection import stereographic_projection
import numpy as np

print(stereographic_projection(np.random.rand(1,4)))
# array([[ 0.14022471,  0.96360618,  0.94909878]])
```

## Installation

### Create a virtual environment

We recommend the use of a Python virtual environment manager like the standard library `venv` or `conda`.

**`venv`**

```bash
  python3 -m venv /<new_env_path>
  source /<new_env_path>/bin/activate
```

**`conda`**

```bash
  conda create -n <new_env_name> python==3.12
  conda activate <new_env_name>
```

### Install projection with pip

After creating the Python virtual environment, you can install the `projection`
package by running the following command from project root directory:

```bash
  pip install -e .
```

### Run the tests

To run the tests, install `projection` package with `test` option which will
install testing dependencies.

```bash
  pip install -e .[test]
```

Then execute the following command from project root directory:

```bash
  pytest
```

#### Test coverage
To run the tests and view coverage report in terminal window:

```bash
  pytest --cov=projection --cov-report term-missing
```

To run the tests and generate coverage report in `html` format:

```bash
  pytest --cov=projection --cov-report=html
```

This will generate coverage report in html format under the `htmlcov` directory
in the project's root directory.

To run the tests and generate coverage report in `xml` format:

```bash
  pytest --cov=projection --cov-report xml:coverage.xml 
```

This will generate coverage report in xml format under in the project's root directory.

It is possible to combine the commands to generate all at once:

```bash
  pytest --cov=projection --cov-report term-missing \
                          --cov-report=html \
                          --cov-report xml:coverage.xml 
```

## Build documentation

To build documentation, install `projection` package with `doc` option which
will install documentation generation dependencies.

```bash
  pip install -e .[doc]
```

Switch to `doc` directory and execute the command below.

```bash
   cd doc
   make html
```

This will generate the documentation under `doc/_build/html` directory in the project's
root directory.

## GitLabCI

During this workshop, we will setup the Continous Integration on this setup by going through these steps:
1. [Fork this project](https://gitlab.inria.fr/formations/integrationcontinue/gitlabciworkshop/-/forks/new) with a correct namespace and visibility level
2. [Setup the CI](practical/2.setup_ci.md) of the forked project
3. [Modify the `.gitlab-ci.yml`](practical/3.yml_for_unitary_tests.md) to launch the unitary tests
4. [Modify the `.gitlab-ci.yml`](practical/4.yml_for_doc_build.md) to build the
documentation and display them through GitLab pages 
5. [Modify the `.gitlab-ci.yml`](practical/5.yml_for_docker.md) to build the docker images and push them to the registry

